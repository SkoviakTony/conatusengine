#include "Unit.h"

Unit::Unit()
{
	direction = true;
	flip = SDL_FLIP_NONE;
	x_position = 0;
	y_position = 0;
	image = nullptr;
	texture = nullptr;
	width = 0;
	height = 0;
	frame_width = 0;
	frame_height = 0;
	frame = 2;
	frame_delay = 11;

	first_zero = false;

	friendly = true;
	
	const int UNIT_VELOCITY = 10;

	//Animation constants
	const int MAX_FRAME_DELAY = 11;
	const int MAX_FRAME = 2;
}

Unit::~Unit()
{
	
}

void Unit::Move_Free(Event_Handler* input)
{
	if (input->Mouse_Down(Event_Handler::MOUSE_LEFT))
	{
		x_position = input->Get_Mouse_X_Position();
		y_position = input->Get_Mouse_Y_Position();
	}

	if (input->Mouse_Hit(Event_Handler::MOUSE_LEFT))
	{
		x_position = input->Get_Mouse_X_Position();
		y_position = input->Get_Mouse_Y_Position();
	}

	if (input->Key_Down(SDL_SCANCODE_UP))
	{	
		//TODO: make screen limits dynamic instead of hardcoded
		if (y_position <= 0)
		{
			y_position = 0;
		}
		else
		{
			y_position -= UNIT_VELOCITY;
		}
	}

	if (input->Key_Down(SDL_SCANCODE_DOWN))
	{
		if (y_position >= 600 - frame_height)
		{
			y_position = 600 - frame_height;
		}
		else
		{
			y_position += UNIT_VELOCITY;
		}
	}
	if (input->Key_Down(SDL_SCANCODE_LEFT))
	{
		if (x_position <= 0)
		{
			x_position = 0;
		}
		else
		{
			x_position -= UNIT_VELOCITY;
		}

		//Flip sprite direction
		if (direction == east)
		{
			flip = SDL_FLIP_NONE;
		}
	}
	if (input->Key_Down(SDL_SCANCODE_RIGHT))
	{
		if (x_position >= 800 - frame_width)
		{
			x_position = 800 - frame_width;
		}
		else
		{
			x_position += UNIT_VELOCITY;
		}

		//Flip sprite direction
		if (direction == east)
		{
			flip = SDL_FLIP_HORIZONTAL;
		}
	}

}

void Unit::Move_Grid(bool map_moving, bool collision, Event_Handler* input)
{
	if (input->Key_Hit(SDL_SCANCODE_UP))
	{
		//TODO: make screen limits dynamic instead of hardcoded
		if (y_position <= 0)
		{
			y_position = 0;
		}
		else if ((map_moving == false || y_position > 200) && collision == false) // 200 = Screen height - 2/3
		{
			y_position -= 32;
		}
	}

	if (input->Key_Hit(SDL_SCANCODE_DOWN))
	{
		if (y_position >= 600 - frame_height)
		{
			y_position = 600 - frame_height;
		}
		else if ((map_moving == false || y_position < 400) && collision == false)// 400 = Screen height - 1/3
		{
			y_position += 32;
		}
	}
	
	if (input->Key_Hit(SDL_SCANCODE_LEFT))
	{
		if (x_position <= 0)
		{
			x_position = 0;
		}
		else if ((map_moving == false || x_position > 200) && collision == false)
		{
			x_position -= 32;
		}

		//Flip sprite direction
		if (direction == west)
		{
			flip = SDL_FLIP_NONE;
		}
	}
	
	if (input->Key_Hit(SDL_SCANCODE_RIGHT))
	{
		if (x_position >= 768 - frame_width)
		{
			x_position = 800 - frame_width;
		}
		else if ((map_moving == false || x_position < 600) && collision == false)
		{
			x_position += 32;
		}

		//Flip sprite direction
		if (direction == east)
		{
			flip = SDL_FLIP_HORIZONTAL;
		}
	}
}

void Unit::Direction(Event_Handler* input)
{

	if (input->Key_Hit(SDL_SCANCODE_UP))
	{
		direction = north;
	}
	if (input->Key_Hit(SDL_SCANCODE_DOWN))
	{
		direction = south;
	}
	if (input->Key_Hit(SDL_SCANCODE_LEFT))
	{
		direction = west;
	}
	if (input->Key_Hit(SDL_SCANCODE_RIGHT))
	{
		direction = east;
	}
	if(input->Key_Hit(SDL_SCANCODE_LEFT) && input->Key_Hit(SDL_SCANCODE_DOWN))
	{
		direction = southwest;
	}
	if (input->Key_Hit(SDL_SCANCODE_RIGHT) && input->Key_Hit(SDL_SCANCODE_DOWN))
	{
		direction = southeast;
	}
	if (input->Key_Hit(SDL_SCANCODE_LEFT) && input->Key_Hit(SDL_SCANCODE_UP))
	{
		direction = northwest;
	}
	if (input->Key_Hit(SDL_SCANCODE_RIGHT) && input->Key_Hit(SDL_SCANCODE_UP))
	{
		direction = northeast;
	}
}

bool Unit::Load_Unit(const char filename[], Render_Manager* rendering)
{
	SDL_Texture* output_image = NULL;
	SDL_Surface* loaded_image = IMG_Load(filename);
	width = loaded_image->w;
	height = loaded_image->h;

	if (loaded_image != NULL)
	{
		Uint32 colorkey = SDL_MapRGB(loaded_image->format, 255, 0, 255);
		SDL_SetColorKey(loaded_image, SDL_TRUE, colorkey);

		output_image = SDL_CreateTextureFromSurface(rendering->Get_Renderer(), loaded_image);
	}
	else
	{
		std::cout << "Image failed to load";
	}

	SDL_FreeSurface(loaded_image);

	texture = output_image;

	return true;
}

bool Unit::Load_Unit_Sheet(const char filename[], Render_Manager* rendering, int file_frame_width, int file_frame_height)
{
	Load_Unit(filename, rendering);
	frame_width = file_frame_width;
	frame_height = file_frame_height;

	return true;
}


bool Unit::Draw_Unit(int x_position, int y_position, Render_Manager* rendering)
{
	SDL_Rect destination;
	destination.x = x_position;
	destination.y = y_position;
	destination.w = width;
	destination.h = height;
	SDL_RenderSetViewport(rendering->Get_Renderer(), &destination);

	SDL_RenderCopy(rendering->Get_Renderer(), texture, NULL, NULL);

	return true;
}

bool Unit::Animate_Unit(Render_Manager* rendering)
{
	//Set destination rectangle (where the image is drawn on the screen), origin at top left.
	SDL_Rect destination;
	destination.x = x_position;
	destination.y = y_position;
	destination.w = frame_width; //Determines the width of the image, in this case it is set to the width of the frame
	destination.h = frame_height;
	//set viewport tells RenderCopy where to start the image
	SDL_RenderSetViewport(rendering->Get_Renderer(), &destination);

	//Variables for determining the number of colums and rows in a sprite sheet (width or height of total sheet / width or height of one frame)
	int columns = width / frame_width;
	int rows = height / frame_height;

	SDL_Rect source;
	source.x = (frame / columns) * frame_width;
	source.y = (frame / rows) * frame_height;
	source.w = frame_width;
	source.h = frame_height;

	//If there are multiple rows
	if (rows == 1) source.y = 0;

	SDL_RenderCopyEx(rendering->Get_Renderer(), texture, &source, NULL, 0, NULL, flip);

	// Set the delay for rendering new images
	frame_delay++;
	if (frame_delay > MAX_FRAME_DELAY)
	{
		frame_delay = 0;
		// Increment image frame by one after delay has passed
		frame++;

		if (frame > MAX_FRAME)
			frame = 0;
	}
	
	return true;
}

void Unit::Free_Surface()
{
	SDL_FreeSurface(image);
	SDL_DestroyTexture(texture);
	texture = NULL;
}

int Unit::Get_X_Position()
{
	return x_position;
}

int Unit::Get_Y_Position()
{
	return y_position;
}

int Unit::Get_Direction()
{
	return direction;
}


void Unit::Set_X_Position(int x_position_in)
{
	x_position = x_position_in;
}

void Unit::Set_Y_Position(int y_position_in)
{
	y_position = y_position_in;
}

int Unit::Get_Width()
{
	return width;
}

int Unit::Get_Height()
{
	return height;
}

int Unit::Get_Frame_Width()
{
	return frame_width;
}

int Unit::Get_Frame_Height()
{
	return frame_height;
}

