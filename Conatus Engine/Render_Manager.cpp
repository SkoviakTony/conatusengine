#include <iostream>
#include <SDL_image.h>
#include "Render_Manager.h"

Render_Manager::Render_Manager()
{
	width = 0;
	height = 0;
	back_buffer = nullptr;
	window = nullptr;
	renderer = nullptr;
}

Render_Manager::~Render_Manager()
{
	
}

bool Render_Manager::Init(int window_width, int window_height, bool fullscreen)
{
	//Initialize window
	if (fullscreen)
	{
		window = SDL_CreateWindow("Sporadic Engine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_FULLSCREEN);
	}
	else
	{
		window = SDL_CreateWindow("Sporadic Engine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_SHOWN);
	}

	//Initialize renderer for window
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL)
	{
		printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
	}
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
	
	//Initialize PNG loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		std::cout << ("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
	}

	back_buffer = SDL_GetWindowSurface(window);		

	return true;
}

void Render_Manager::Clear_Renderer()
{
	SDL_RenderClear(renderer);
}

void Render_Manager::Render_Image()
{
	SDL_RenderPresent(renderer);
}


void Render_Manager::Update_Front_Buffer()
{
	SDL_UpdateWindowSurface(window);
}

void Render_Manager::Clear_Back_Buffer(Uint32 r, Uint32 g, Uint32 b)
{
	if (back_buffer == NULL)
		return;
	
		Uint32 color;

		color = SDL_MapRGB(back_buffer->format, r, g, b);

		SDL_FillRect(back_buffer, NULL, color);
}


SDL_Surface* Render_Manager::Get_Back_Buffer()
{
	return back_buffer;
}

SDL_Renderer* Render_Manager::Get_Renderer()
{
	return renderer;
}


