#include "Map.h"

Map::Map()
{
	width = 0;
	height = 0;
	tile_width = 0;
	tile_height = 0;
	tile_texture = nullptr;
	map_x = 0;
	map_y = 0;
	start_row = 0;
	start_column = 0;
	map_moving = false;
}

Map::~Map()
= default;

bool Map::Load_Map(const char filename[], Render_Manager* rendering)
{
	SDL_Texture* output_image = NULL;
	SDL_Surface* loaded_image = IMG_Load(filename);
	width = loaded_image->w;
	height = loaded_image->h;

	if (!loaded_image == NULL)
	{
		Uint32 colorkey = SDL_MapRGB(loaded_image->format, 255, 0, 255);
		SDL_SetColorKey(loaded_image, SDL_TRUE, colorkey);

		output_image = SDL_CreateTextureFromSurface(rendering->Get_Renderer(), loaded_image);
	}
	else
	{
		std::cout << "Image failed to load";
	}

	SDL_FreeSurface(loaded_image);

	tile_texture = output_image;

	return true;
}

bool Map::Load_Tile_Map(const char tile_sheet[], const char map_path[], int file_tile_width, int file_tile_height, Render_Manager* rendering)
{
	Load_Map(tile_sheet, rendering);
	tile_width = file_tile_width;
	tile_height = file_tile_height;

	std::fstream map_file;
	map_file.open(map_path);
	int k;

	for (int i = 0; i < MAP_ROWS; i++)
	{
		for (int j = 0; j < MAP_COLUMNS; j++)
		{
			map_file >> k;
			map[i][j] = new Tile(k);
		}
	}

	map_file.close();
	
	return true;
}


bool Map::Draw_Map(int x_pos, int y_pos, Render_Manager* rendering)
{
	SDL_Rect destination;
	destination.x = x_pos;
	destination.y = y_pos;
	destination.w = width;
	destination.h = height;
	SDL_RenderSetViewport(rendering->Get_Renderer(), &destination);

	SDL_RenderCopy(rendering->Get_Renderer(), tile_texture, NULL, NULL);

	return true;
}

bool Map::Draw_Tile_Map(Render_Manager* rendering)
{	
	
	//Set destination rectangle (where the image is drawn on the screen), origin at top left.
	SDL_Rect destination;
	destination.x = 0;
	destination.y = 0;
	destination.w = tile_width; //Determines the width of the image, in this case it is set to the width of the frame
	destination.h = tile_height;

	//Variables for determining the number of colums and rows in a sprite sheet (width or height of total sheet / width or height of one frame)
	int tile_sheet_columns = width / tile_width;
	int tile_sheet_rows = height / tile_height;

	SDL_Rect source;
	source.w = tile_width;
	source.h = tile_height;
	source.x = 0;
	source.y = 0;

	

	int row = start_row;
	int column = start_column;


	const int CURRENT_ROW = map[row][column]->Get_Tile_ID() / tile_sheet_columns;

	while (row < (start_row + (632/tile_height)))
	{
		while (column < (start_column + (800/tile_width)))
		{		
			SDL_RenderSetViewport(rendering->Get_Renderer(), &destination);

				if(map[row][column]->Get_Tile_ID() >= tile_sheet_columns + 1)
					{
						source.y = CURRENT_ROW * tile_height;
						source.x = ((map[row][column]->Get_Tile_ID() - CURRENT_ROW*(tile_sheet_columns)) * tile_width - 1);
					}
				else
					{
						source.x = map[row][column]->Get_Tile_ID() * tile_width;
						source.y = 0;
					}
			
			SDL_RenderCopy(rendering->Get_Renderer(), tile_texture, &source, NULL);
			column++;

			destination.x += tile_width;	
		}
		destination.x = 0;
		destination.y += tile_height;
		row++;
		column = start_column;
	}	
	
	return true;
}

void Map::Move_Map(Unit* unit, Event_Handler* input)
{
	int player_x = unit->Get_X_Position();
	int player_y = unit->Get_Y_Position();
	
	if (input->Key_Hit(SDL_SCANCODE_UP))
	{
		//TODO: Fix hardcoded map width and height
		if (start_row <= 0) 
		{
			start_row = 0;
			map_moving = false;
		}
		else if (player_y < 200 && Collision_Detection(unit) == false) // 200 = screen height - 2/3
		{
			start_row -= 1;
			map_moving = true;
		}
		
	}	

	if (input->Key_Hit(SDL_SCANCODE_DOWN))
	{
		//TODO: Fix hardcoded map width and height
		if (start_row >= MAP_ROWS - (600 + tile_height) / tile_height) //total height in rows of the map minus the screen height in rows
		{
			start_row = MAP_ROWS - (600 + tile_height) / tile_height;
			map_moving = false;
		}
		else if (player_y > 400 && Collision_Detection(unit) == false) // 400 = screen height - 1/3
		{
			start_row += 1;
			map_moving = true;
		}
	}

	if (input->Key_Hit(SDL_SCANCODE_LEFT))
	{
		//TODO: Fix hardcoded map width and height
		if (start_column <= 0)
		{
			start_column = 0;
			map_moving = false;
		}
		else if (player_x < 200 && Collision_Detection(unit) == false) // = screen width - 3/4
		{
			start_column -= 1;
			map_moving = true;
		}
	}

	if (input->Key_Hit(SDL_SCANCODE_RIGHT))
	{
		//TODO: Fix hardcoded map width and height
		if (start_column >= MAP_COLUMNS - (768 + tile_width) / tile_width) //total height in rows of the map minus the screen height in rows
		{
			start_column = MAP_COLUMNS - (768 + tile_width) / tile_width;
			map_moving = false;
		}
		else if (player_x > 600 && Collision_Detection(unit) == false) // = screen width - 1/4
		{
			start_column += 1;
			map_moving = true;
		}
	}
}

bool Map::Collision_Detection(Unit* unit)
{
	int direction = unit->Get_Direction();
	int unit_x = unit->Get_X_Position();
	int unit_y = unit->Get_Y_Position();

	switch(direction)
	{
		case north:
			unit_y -= 32;
			break;
		case east:
			unit_x += 32;
			break;
		case south:
			unit_y += 32;
			break;
		case west:
			unit_x -= 32;
			break;
		case northwest:
			unit_x -= 32;
			unit_y -= 32;
			break;
		case northeast:
			unit_x += 32;
			unit_y -= 32;
			break;
		case southwest:
			unit_x -= 32;
			unit_y += 32;
			break;
		case southeast:
			unit_x += 32;
			unit_y += 32;
			break;
		default:
			break;
	}
	
	int unit_row = start_row + unit_y / 32;
	int unit_column = start_column + unit_x / 32;
	int terrain_type = 0;

	if(unit_x > -1 && unit_x < 801 && unit_y > -1 && unit_y < 601)
	{
		terrain_type = map[unit_row][unit_column]->Get_Terrain_Type();
	}

	
	if (terrain_type == water)
	{
		return true;
	}
		return false;
}

void Map::Free_Surface()
{
	//SDL_FreeSurface(image);
	SDL_DestroyTexture(tile_texture);
	tile_texture = NULL;
}

int Map::Get_Start_Row() {
	return start_row;
}

int Map::Get_Start_Column() {
	return start_column;
}

bool Map::Get_Map_Moving()
{
	return map_moving;
}
