#include "Enums.h"

class Tile
{
public:
	Tile(int tile_id_in);
	~Tile();

	int Get_Tile_ID();
	int Get_Terrain_Type();
private:
	int tile_id; //ID of the tile, e.g. 0 = dirt, 1 = grass, 2 = water
	int terrain_type;
};

