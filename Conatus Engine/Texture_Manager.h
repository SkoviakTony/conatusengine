#pragma once
#include <SDL.h>
#include "Render_Manager.h"
#include "Unit.h"

class Texture_Manager
{
public:
	Texture_Manager();
	~Texture_Manager();

	bool Load_Image(const char filename[], Render_Manager* rendering);
	bool Load_Image_Sheet(const char filename[], int file_frame_width, int file_frame_height, Render_Manager* rendering);
	
	//Texture methods
	void Draw_Texture_To_Back_Buffer(int x_pos, int y_pos, Render_Manager* rendering);
	void Draw_Texture_Frame_To_Back_Buffer(Unit* unit, int frame, Render_Manager* rendering, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Blitting methods
	bool Draw_To_Back_Buffer(int x_pos, int y_pos, Render_Manager* rendering);
	bool Draw_Frame_To_Back_Buffer(int x_pos, int y_pos, int frame, Render_Manager* rendering);

	//Destroy textures and free surfaces
	bool Free_Surface();


	int Get_Width();
	int Get_Height();
	int Get_Frame_Width();
	int Get_Frame_Height();
	
private:
	SDL_Surface* image;
	SDL_Texture* texture;

	int width;
	int height;
	int frame_width;
	int frame_height;
};

