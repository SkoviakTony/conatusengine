#include <cstdio>
#include <iostream>
#include <vector>
#include "Render_Manager.h"
#include "Event_Handler.h"
#include "Audio.h"
#include "Map.h"
#include "Music.h"
#include "Sound_Effects.h"
#include "Unit.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int FULLSCREEN = false;

//Low-level systems
Render_Manager rendering;
Event_Handler input;
Audio audio;
Music music;
Sound_Effects sound_effect[5];

//Graphics
Map background;
Map tilemap;

std::vector<Unit> heroes;
Unit hero;
Unit hero2;


bool Init();
bool Program_Running();
void Free_Program();

int main(int argc, char* args[])
{
    if(!Init()) //Initialize systems
    {
        std::cout << "Error, failed to initialize" << std::endl;
        return false;
    }

    int hero_num = 1;
	
    while (Program_Running())
    {
    
        rendering.Clear_Renderer();

        Mix_VolumeMusic(30);
        if (!audio.Music_Playing()) music.Play_Music(-1);

	    input.Update();
	    if (input.Key_Down(SDL_SCANCODE_ESCAPE)) break;

        background.Draw_Map(0, 0, &rendering);
        tilemap.Draw_Tile_Map(&rendering);

        if (input.Key_Hit(SDL_SCANCODE_SPACE) && hero_num == 1)
        {
            hero_num = 2;
        }
        else if (input.Key_Hit(SDL_SCANCODE_SPACE) && hero_num == 2)
        {
            hero_num = 1;
        }
    	
    	if(hero_num == 1)
    	{
            hero.Direction(&input);
            hero.Move_Grid(tilemap.Get_Map_Moving(), tilemap.Collision_Detection(&hero), &input);
            tilemap.Move_Map(&hero, &input);
    	}
        
        if (hero_num == 2)
        {
            hero2.Direction(&input);
            hero2.Move_Grid(tilemap.Get_Map_Moving(), tilemap.Collision_Detection(&hero2), &input);
            tilemap.Move_Map(&hero2, &input);
        }
    	
        hero.Animate_Unit(&rendering);
        hero2.Animate_Unit(&rendering);
    	
    	rendering.Render_Image();

    }

    return 0;
}

bool Init()
{
    //Initialization flag
    bool success = true;

    //Initialize SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
        return false;

	//Initialize screen
    if(!rendering.Init(SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN))
        return false;

	//Initialize audio
    if (!audio.Init())
        return false;

	//Load images
	if(!background.Load_Map("assets/background.png", &rendering))
        return false;

    if (!tilemap.Load_Tile_Map("assets/map_set.png", "assets/map.txt", 32, 32, &rendering))
        return false;

	if (!hero.Load_Unit_Sheet("assets/hero.png", &rendering, 24, 24))
		return false;

	if (!hero2.Load_Unit_Sheet("assets/hero2.png", &rendering, 23, 24))
		return false;

    //Load music and SFX
    if (!music.Load_Music("audio/loop.wav"))
        return false;

    if (!sound_effect[0].Load_Sound_Effect("audio/sfx/flying.wav"))
        return false;
	
	//Initialize input arrays
    input.Init();
	
    return success;
}

bool Program_Running()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        //User requests quit
        if (event.type == SDL_QUIT)
        {
            return false;
        }
    }
    return true;
}

void Free_Program()
{
    music.Free_Music();

    for (int i = 0; i < 5; i++)
        sound_effect[i].Free_Sound_Effect();
	
    hero.Free_Surface();
    hero2.Free_Surface();
    background.Free_Surface();
	
    //TODO: rendering.Kill();
    input.Kill();
    audio.Kill();
    SDL_Quit();
}