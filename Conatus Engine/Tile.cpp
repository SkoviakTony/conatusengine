#include "Tile.h"

Tile::Tile(int tile_id_in)
{
	tile_id = tile_id_in;

	if(tile_id == 2)
	{
		terrain_type = water;
	}
	else
	{
		terrain_type = normal;
	}
}

Tile::~Tile()
{
	
}

int Tile::Get_Tile_ID()
{
	return tile_id;
}

int Tile::Get_Terrain_Type()
{
	return terrain_type;
}