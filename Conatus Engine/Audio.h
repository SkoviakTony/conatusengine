#pragma once

#include <SDL.h>
#include <SDL_mixer.h>
#include <stdio.h>

class Audio
{
public:
	bool Init();
	void Kill();

	bool Music_Playing();
	bool Music_Paused();
	void Pause_Music();
	void Resume_Music();
	void Stop_Music();
	
	void Stop_Channel(int channel);
private:
	
};

