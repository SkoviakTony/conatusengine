#pragma once

enum terrain_type
{
	normal,
	water,
	wall
};

enum direction
{
	north,
	northeast,
	east,
	southeast,
	south,
	southwest,
	west,
	northwest
};