#include "Music.h"

#include <iostream>

Music::Music()
{
	music = NULL;
}

Music::~Music()
{
	
}

bool Music::Load_Music(const char filename[])
{
	music = Mix_LoadMUS(filename);
	
	if (music == NULL)
	{
		std::cout << "Failed to load music:" << filename << "\n";
	}

	return true;
}

void Music::Free_Music()
{
	if(music != NULL)
	{
		Mix_FreeMusic(music);
		music = NULL;
	}
}

void Music::Play_Music(int loops)
{
	if (music != NULL)
		Mix_PlayMusic(music, loops);
}

bool Music::Is_Loaded()
{
	return(music != NULL);
}
