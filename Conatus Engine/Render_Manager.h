#pragma once
#include <SDL.h>

class Render_Manager
{
public:
	Render_Manager();
	~Render_Manager();
	
	bool Init(int window_width, int window_height, bool fullscreen);

	//Texture methods
	void Clear_Renderer();
	void Render_Image();
	
	//Blitting methods
	void Update_Front_Buffer();
	void Clear_Back_Buffer(Uint32 r, Uint32 g, Uint32 b);
	SDL_Surface* Get_Back_Buffer();
	SDL_Renderer* Get_Renderer();
	
private:
	int width;
	int height;

	SDL_Renderer* renderer;
	SDL_Window* window;
	SDL_Surface* back_buffer;
};

