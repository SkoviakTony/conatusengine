#pragma once
#include "Audio.h"

class Music
{
public:
	Music();
	~Music();

	bool Load_Music(const char filename[]);
	void Free_Music();
	void Play_Music(int loops = 0);
	bool Is_Loaded();
	
private:
	Mix_Music* music;
};





////MUSIC TOGGLE
 //   if(input.Key_Hit(SDL_SCANCODE_SPACE))
 //   {
	//    if(!audio.Music_Playing())
	//    {
	   //     music.Play_Music(-1);
	//    }
 //       else
 //       {
	//        if(audio.Music_Paused())
	//        {
	   //         audio.Resume_Music();
	//        }
	//        else
	//        {
	   //         audio.Pause_Music();
	//        }
 //       }
 //   }