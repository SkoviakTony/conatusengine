#pragma once
#include <SDL.h>

class Event_Handler
{
public:
	static const int MOUSE_LEFT = 1;
	static const int MOUSE_RIGHT = 2;
	static const int MOUSE_MIDDLE = 3;

	void Init();
	void Kill();
	void Update();

	bool Key_Down(int key);
	bool Key_Hit(int key);
	bool Key_Up(int key);

	bool Mouse_Down(int key);
	bool Mouse_Hit(int key);
	bool Mouse_Up(int key);

	int Get_Mouse_X_Position();
	int Get_Mouse_Y_Position();

	void Hide_Cursor(bool hide = true);
	
private:
	bool* keys; //on press
	bool* previous_keys; //on release
	
	bool mouse_keys[3];
	bool previous_mouse_keys[3];

	int number_of_keys;
	int mouse_x_position;
	int mouse_y_position;
};

