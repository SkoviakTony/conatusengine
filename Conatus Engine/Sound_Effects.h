#pragma once
#include "Audio.h"

class Sound_Effects
{
public:
	Sound_Effects();
	~Sound_Effects();
	bool Load_Sound_Effect(const char filename[]);
	void Free_Sound_Effect();

	int Play_Sound_Effect(int loops = 0);
	bool Is_Loaded();
private:
	Mix_Chunk* sound;	
};

