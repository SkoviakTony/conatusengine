#include "Sound_Effects.h"

#include <iostream>

Sound_Effects::Sound_Effects()
{
	sound = nullptr;
}

Sound_Effects::~Sound_Effects()
{
	
}

bool Sound_Effects::Load_Sound_Effect(const char filename[])
{
	sound = Mix_LoadWAV(filename);

	if(sound == NULL)
	{
		std::cout << "Failed to load sound:" << filename << "\n";
	}

	return true;
}

void Sound_Effects::Free_Sound_Effect()
{
	if(sound != NULL)
	{
		Mix_FreeChunk(sound);
		sound = NULL;
	}
	
}

int Sound_Effects::Play_Sound_Effect(int loops)
{
	if(sound != NULL)
	{
		return Mix_PlayChannel(-1, sound, loops);
	}
	else
	{
		return -1;
	}
}

bool Sound_Effects::Is_Loaded()
{
	return (sound != NULL);
}


