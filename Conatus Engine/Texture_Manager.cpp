#include <iostream>

#include "Texture_Manager.h"
#include <SDL_image.h>

Texture_Manager::Texture_Manager()
{
	image = nullptr;
	texture = nullptr;
	width = 0;
	height = 0;
	frame_width = 0;
	frame_height = 0;
}

Texture_Manager::~Texture_Manager(){}

bool Texture_Manager::Load_Image(const char filename[], Render_Manager* rendering)
{
	SDL_Texture* output_image;
	SDL_Surface* loaded_image = IMG_Load(filename);
	width = loaded_image->w;
	height = loaded_image->h;
	
	if(!loaded_image == NULL)
	{
		Uint32 colorkey = SDL_MapRGB(loaded_image->format, 255, 0, 255);
		SDL_SetColorKey(loaded_image, SDL_TRUE, colorkey);
		
		output_image = SDL_CreateTextureFromSurface(rendering->Get_Renderer(), loaded_image);
	}
	else
	{
		std::cout << "Image failed to load";
		return false;
	}

	SDL_FreeSurface(loaded_image);
	
	texture = output_image;

	
	//SDL_Surface* optimized_image = NULL;
	//
	//SDL_Surface* loaded_image = IMG_Load(filename);

	//width = loaded_image->w;
	//height = loaded_image->h;

	////Convert surface to screen format
	//optimized_image = SDL_ConvertSurface(loaded_image, rendering->Get_Back_Buffer()->format, 0);
	//if (optimized_image == NULL)
	//{
	//	std::cout << ("Unable to optimize image %s! SDL Error: %s\n", /*path.c_str(),*/ SDL_GetError());
	//}
	//

	return true;
}


bool Texture_Manager::Load_Image_Sheet(const char filename[], int file_frame_width, int file_frame_height, Render_Manager* rendering)
{
	if(Load_Image(filename, rendering))
	{
		frame_width = file_frame_width;
		frame_height = file_frame_height;
		
		return true;
	}

	
	return false;
}

void Texture_Manager::Draw_Texture_To_Back_Buffer(int x_pos, int y_pos, Render_Manager* rendering)
{
	SDL_Rect destination;
	destination.x = x_pos;
	destination.y = y_pos;
	destination.w = width;
	destination.h = height;
	SDL_RenderSetViewport(rendering->Get_Renderer(), &destination);
	
	SDL_RenderCopy(rendering->Get_Renderer(), texture, NULL, NULL);
}

void Texture_Manager::Draw_Texture_Frame_To_Back_Buffer(Unit* unit, int frame, Render_Manager* rendering, SDL_RendererFlip flip)
{
	//Set destination rectangle (where the image is drawn on the screen), origin at top left.
	SDL_Rect destination;
	destination.x = unit->Get_X_Position();
	destination.y = unit->Get_Y_Position();
	destination.w = frame_width; //Determines the width of the image, in this case it is set to the width of the frame
	destination.h = frame_height;
	//set viewport tells RenderCopy where to start the image
	SDL_RenderSetViewport(rendering->Get_Renderer(), &destination);

	//Variables for determining the number of colums and rows in a sprite sheet (width or height of total sheet / width or height of one frame)
	int columns = width / frame_width;
	int rows = height / frame_height;
	
	SDL_Rect source;
	source.x = (frame / columns) * frame_width;
	source.y = (frame / rows) * frame_height;
	source.w = frame_width;
	source.h = frame_height;

	//If there are multiple rows
	if (rows == 1) source.y = 0;

	SDL_RenderCopyEx(rendering->Get_Renderer(), 
					 texture, 
					 &source, 
					 NULL,
					 0,
					 NULL,
					 flip);
}


bool Texture_Manager::Draw_To_Back_Buffer(int x_pos, int y_pos, Render_Manager* rendering)
{
	SDL_Rect destination;
	destination.x = x_pos;
	destination.y = y_pos;
	
	SDL_BlitSurface(image, NULL, rendering->Get_Back_Buffer(), &destination);

	return true;
}

bool Texture_Manager::Draw_Frame_To_Back_Buffer(int x_pos, int y_pos, int frame, Render_Manager* rendering)// , SDL_RendererFlip flip)
{
	SDL_Rect destination;
	destination.x = x_pos;
	destination.y = y_pos;

	int columns = width / frame_width;
	int rows = height / frame_height;

	SDL_Rect source;
	source.x = (frame / columns)* frame_width;
	source.y = (frame / rows)* frame_height;
	source.w = frame_width;
	source.h = frame_height;

	if (rows == 1) source.y = 0;
	
	SDL_BlitSurface(image, &source, rendering->Get_Back_Buffer(), &destination);

	
	//SDL_RenderCopyEx(rendering, rendering->Get_Back_Buffer(), &source, &destination, angle, center, flip);

	return true;
}


bool Texture_Manager::Free_Surface()
{
	SDL_FreeSurface(image);
	SDL_DestroyTexture(texture);
	texture = NULL;

	
	return true;
}




int Texture_Manager::Get_Width()
{
	return width;
}

int Texture_Manager::Get_Height()
{
	return height;
}

int Texture_Manager::Get_Frame_Width()
{
	return frame_width;
}

int Texture_Manager::Get_Frame_Height()
{
	return frame_height;
}