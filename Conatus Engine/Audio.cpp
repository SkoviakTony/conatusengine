#include "Audio.h"
#include <iostream>

bool Audio::Init()
{
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 2048) == -1)
	{
		std::cout << "Failed to initialize audio\n";
		return false;
	}

	return true;
}

void Audio::Kill()
{
	Mix_CloseAudio();
}

bool Audio::Music_Playing()
{
	return Mix_PlayingMusic();
}

bool Audio::Music_Paused()
{
	return Mix_PausedMusic();
}

void Audio::Pause_Music()
{
	Mix_PauseMusic();
}

void Audio::Resume_Music()
{
	Mix_ResumeMusic();
}

void Audio::Stop_Music()
{
	Mix_HaltMusic();
}

void Audio::Stop_Channel(int channel)
{
	Mix_HaltChannel(channel);
}






