#include "Event_Handler.h"

void Event_Handler::Init()
{
	Uint8* keyboard = (Uint8*)SDL_GetKeyboardState(&number_of_keys); //keyboard int holds the value of any key that gets pressed

	keys = new bool[number_of_keys]; //new puts these values on the heap
	previous_keys = new bool[number_of_keys];

	for (int i = 0; i < number_of_keys; i++) //loops through the number of keys and initializes each key to a number (starting from 1 (or is it 0?), counting up with i)
	{
		keys[i] = keyboard[i];
		previous_keys[i] = false;
	}

	SDL_GetMouseState(&mouse_x_position, &mouse_y_position);

	for (int i = 0; i <= 2; i++)
	{
		mouse_keys[i] = SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(i);
		previous_mouse_keys[i] = false;
	}
}

void Event_Handler::Kill()
{
	delete[] keys;
	delete[] previous_keys;
}

void Event_Handler::Update()
{
	Uint8* keyboard = (Uint8*)SDL_GetKeyboardState(&number_of_keys);

	for(int i = 0; i < number_of_keys; i++)
	{
		previous_keys[i] = keys[i];
		keys[i] = keyboard[i];
	}

	SDL_GetMouseState(&mouse_x_position, &mouse_y_position);

	for (int i = 0; i <= 2; i++)
	{
		previous_mouse_keys[i] = mouse_keys[i];
		mouse_keys[i] = SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(i);
	}
}

bool Event_Handler::Key_Down(int key)
{
	if (key < 0 || key > number_of_keys) 
		return false; //If the key is out of the bounds of our array, return false; it hasn't been initialized

	return keys[key]; //When key is pressed
}

bool Event_Handler::Key_Hit(int key)
{
	if (key < 0 || key > number_of_keys) 
		return false;
	
	return keys[key] && !previous_keys[key]; //When key is pressed and not released
}

bool Event_Handler::Key_Up(int key)
{
	if (key < 0 || key > number_of_keys)
		return false;

	return (previous_keys[key] && !keys[key]); //When key is released and not pressed
}

bool Event_Handler::Mouse_Down(int key)
{
	if (key < 0 || key > 3)
		return false;

	return mouse_keys[key]; //When key is pressed
}

bool Event_Handler::Mouse_Hit(int key)
{
	if (key < 0 || key > 3)
		return false;

	return mouse_keys[key] && !previous_mouse_keys[key]; //When key is pressed and not released
}

bool Event_Handler::Mouse_Up(int key)
{
	if (key < 0 || key > 3)
		return false;

	return previous_mouse_keys[key] && !mouse_keys[key]; //When key is released and not pressed
}

int Event_Handler::Get_Mouse_X_Position()
{
	return mouse_x_position;
}

int Event_Handler::Get_Mouse_Y_Position()
{
	return mouse_y_position;
}

void Event_Handler::Hide_Cursor(bool hide)
{
	if (hide)
		SDL_ShowCursor(SDL_DISABLE);
	else
		SDL_ShowCursor(SDL_ENABLE);
}