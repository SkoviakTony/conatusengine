#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <fstream>
#include <iostream>
#include "Render_Manager.h"
#include "Event_Handler.h"
#include "Tile.h"
#include "Enums.h"
#include "Unit.h"

class Map
{
public:
	Map();
	~Map();

	//For tile_texture maps
	bool Load_Tile_Map(const char tile_sheet[], const char map_path[], int tile_width, int tile_height, Render_Manager* rendering);
	bool Draw_Tile_Map(Render_Manager* rendering);
	
	//For whole images
	bool Load_Map(const char filename[], Render_Manager* rendering);
	bool Draw_Map(int x_pos, int y_pos, Render_Manager* rendering);

	void Move_Map(Unit* unit, Event_Handler* input);
	bool Collision_Detection(Unit* unit);
	
	void Free_Surface();

	int Get_Start_Row();
	int Get_Start_Column();

	bool Get_Map_Moving();

private:
	int tile_width, tile_height;
	int width, height;
	
	SDL_Texture* tile_texture;

	//For scrolling the map
	int start_row, start_column;
	int map_x, map_y;
	static const int SCROLL_SPEED = 32;
	bool map_moving;

	//Map variables
	static const int MAP_COLUMNS = 40;
	static const int MAP_ROWS = 27;
	//int map[MAP_ROWS][MAP_COLUMNS];
	Tile* map[MAP_ROWS][MAP_COLUMNS];
};

