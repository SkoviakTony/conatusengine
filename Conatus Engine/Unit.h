#pragma once
#include "Event_Handler.h"
#include "Render_Manager.h"
#include "Enums.h"
#include <iostream>
#include <SDL_image.h>

class Unit
{
public:
	Unit();
	~Unit();

	const int UNIT_VELOCITY = 7;
	const int MAX_FRAME_DELAY = 11;
	const int MAX_FRAME = 2;

	//Movement methods
	void Move_Free(Event_Handler* input);
	void Move_Grid(bool map_moving, bool collision, Event_Handler* input);
	void Direction(Event_Handler* input);

	//Load unit graphics
	bool Load_Unit(const char filename[], Render_Manager* rendering);
	bool Load_Unit_Sheet(const char filename[], Render_Manager* rendering, int file_frame_width, int file_frame_height);

	//Draw unit to back buffer
	bool Draw_Unit(int x_position, int y_position, Render_Manager* rendering);
	bool Animate_Unit(Render_Manager* rendering);

	//Destroy textures and free surfaces
	void Free_Surface();

	/*void Animate_Unit(int frame, Render_Manager* rendering, Texture_Manager* texture, SDL_RendererFlip flip = SDL_FLIP_NONE);*/
	
	int Get_X_Position();
	int Get_Y_Position();
	int Get_Direction();

	void Set_X_Position(int x_position_in);
	void Set_Y_Position(int y_position_in);

	int Get_Width();
	int Get_Height();
	int Get_Frame_Width();
	int Get_Frame_Height();

private:
	// Unit position
	int x_position, y_position;
	int direction;

	bool first_zero;
	
	//Rendering and Animation
	SDL_Surface* image;
	SDL_Texture* texture;

	bool friendly;
	

	int width;
	int height;
	int frame_width;
	int frame_height;
	
	int frame, frame_delay;
	SDL_RendererFlip flip;
};

